#!/usr/bin/env python3
from argparse import (
    Action,
    ArgumentParser,
)
from base64 import b64encode
from functools import partial
from hashlib import sha256
from os import (
    chdir,
    environ,
)
from pathlib import Path
from re import (
    search,
    sub,
)
from shutil import (
    copy2,
    copytree,
    rmtree,
)
from subprocess import (
    CalledProcessError,
    run,
)
from sys import stderr
from tarfile import open as taropen
from tempfile import mkdtemp


class StoreAsDictKeyValue(Action):
    def __init__(self, option_strings, **kwargs):
        if 'dict_key' not in kwargs:
            raise ValueError('dict_key should be specified when store is StoreAsDictKeyValue')
        self.dict_key = kwargs.pop('dict_key')
        kwargs['default'] = dict()
        super().__init__(option_strings, **kwargs)

    def __call__(self, parser, namespace, value, option_string=None):
        getattr(namespace, self.dest)[self.dict_key] = value


def grep(expr, file_path, as_str=True):
    with open(file_path) as f:
        lines = [line for line in f if search(expr, line)]
        return ''.join(lines).strip() if as_str else lines


def create_release(parser, env, secrets, ssh_key_path, components):
    tmp_dir = Path(mkdtemp())
    try:
        os_env = {k: v for k, v in environ.items()}
        os_env.update({f'{comp.upper()}_VERSION': ver for comp, ver in components.items() if ver is not None})
        os_env['TARGET_ENV'] = env
        os_env['PYTHONUNBUFFERED'] = '1'
        create_context(tmp_dir, env, secrets, ssh_key_path, components, os_env)
        if Path('proxy').is_dir():
            copytree('proxy', tmp_dir / 'proxy')
        app_name = sub(r'app=', '', grep(r'^app=', tmp_dir / 'metadata'))
        rlz_name = f'{app_name}--{env}' + ''.join(f'--{ver}' for ver in components.values()) + '.dca'
        rlz_dir = tmp_dir.cwd().resolve() / 'releases'
        if not rlz_dir.exists():
            rlz_dir.mkdir()
        rlz_path = rlz_dir / rlz_name
        create_dca(rlz_path, tmp_dir)
        create_sha256(rlz_path)
        return rlz_path
    finally:
        rmtree(tmp_dir)


def create_context(tmp_dir, env, secrets, ssh_key_path, components, os_env):
    ctx_dir = tmp_dir / 'context'
    copytree('context', ctx_dir)
    with open('metadata') as fr:
        with open(tmp_dir / 'metadata', 'w') as fw:
            fw.write(f'target_env={env}\n')
            for line in fr:
                fw.write(line)
            fw.write('\n')
            for component, version in components.items():
                if version:
                    fw.write(f'{component}_version={version}\n')
    if secrets:
        extra_context = secrets.glob('context/*')
        for f in extra_context:
            if f.is_dir():
                copytree(f, ctx_dir / f.name)
            else:
                copy2(f, ctx_dir)
    run(['docker-compose', 'config', '-q'], check=True, capture_output=True, cwd=ctx_dir, env=os_env)
    docker_compose_path = ctx_dir / 'docker-compose.yml'
    with open(docker_compose_path, 'r+') as f:
        dc = f.read()
        for key, value in sorted(os_env.items(), reverse=True):
            dc = sub(r'\$\{?' + key + r'\}?', value, dc)
        f.seek(0)
        f.truncate()
        f.write(dc)
    if ssh_key_path:
        sign = run([
            'openssl', 'dgst',
            '-sha256',
            '-sign', str(ssh_key_path),
            str(docker_compose_path),
        ], check=True, capture_output=True).stdout
        sign_base64 = b64encode(sign).decode('ascii')
        with open('metadata') as fr:
            with open(tmp_dir / 'metadata', 'w') as fw:
                fw.write(f'target_env={env}\n')
                fw.write(f'signature={sign_base64}\n')
                for line in fr:
                    fw.write(line)
                fw.write('\n')


def create_dca(rlz_path, tmp_dir):
    print(f'CREATING {rlz_path}')
    with taropen(rlz_path, 'w:gz') as tar:
        tar.add(tmp_dir, arcname='.')


def create_sha256(rlz_path):
    CHUNK_SIZE = 1024 * 1024  # 1 MB
    with open(rlz_path, 'rb') as f1:
        with open(rlz_path.with_suffix(rlz_path.suffix + '.sha256'), 'w') as f2:
            h = sha256()
            for chunk in iter(partial(f1.read, CHUNK_SIZE), b''):
                h.update(chunk)
            f2.write(f'{h.hexdigest()}  {rlz_path.name}\n')


def deploy_dca(parser, rlz_path):
    server_host = environ['APP_SERVER_HOST']
    print(f'DEPLOYING {rlz_path} on {server_host}')
    run(['scp', '-O', '-P', '122', f'{rlz_path}', f'{rlz_path}.sha256', f'dca@{server_host}:'], check=True, capture_output=True)
    run(['ssh', f'deploy@{server_host}', 'deploy', rlz_path.name], check=True, capture_output=True)
    print('DEPLOYED')


def main(sys_args):
    here = Path(__file__).resolve().parent
    chdir(here)
    app_name = sub(r'app=', '', grep(r'^app=', 'metadata'))
    privileged = sub(r'privileged=', '', grep(r'^privileged=', 'metadata')) == '1'
    parser = ArgumentParser(
        description=f"Create a DCA release for {app_name}",
        epilog="""
        If confs/ENV/context/ exists its contents will be copied
        to the regular context directory.
        If the same file exists in both directories, the file in
        confs/ENV/context/ will squash the other one.

        APP_SERVER_HOST variable should be defined to where to deploy if -d is used.""",
    )
    if privileged:
        parser.add_argument(
            'ssl_key',
            help="your private openssl key file path to sign this release")
    parser.add_argument(
        'env',
        choices=['dev', 'integ', 'staging', 'demo', 'prod'],
        help="confs/ENV is the expected secret directory for each environment")
    parser.add_argument(
        '-d', '--deploy', dest='dodeploy', action='store_true',
        help="deploy the DCA. You must check your rights to do so and have APP_SERVER_HOST defined.")
    components_str = Path('components').read_text(encoding='utf8').strip()
    components = components_str.split('\n') if components_str else []
    for component in components:
        parser.add_argument(f'--{component}', metavar='VERSION', required=True,
                            dest='comps', dict_key=component, action=StoreAsDictKeyValue,
                            help=f"docker image version of {app_name} {component}")
    args = parser.parse_args(args=sys_args)
    secrets = Path('.') / 'confs' / args.env
    if secrets.is_dir():
        secrets_path = secrets.resolve()
    else:
        print(f'No secret for {args.env} environment')
        secrets_path = None
    if privileged:
        private_key = Path(args.ssl_key)
        if not private_key.exists():
            parser.error(f"{args.ssl_key} should be an existing openssl private key file")
    else:
        private_key = None
    comps = {comp: args.comps[comp] for comp in components}
    try:
        rlz_path = create_release(parser, args.env, secrets_path, private_key, comps)
        if args.dodeploy:
            deploy_dca(parser, rlz_path)
    except CalledProcessError as e:
        output = (e.stdout or '') + (e.stderr or '')
        print(e, file=stderr)
        if output:
            print(output, file=stderr)


if __name__ == '__main__':
    from sys import argv
    main(argv[1:])
